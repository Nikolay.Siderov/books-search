import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { APP_ROUTES } from './core/constants';

const appRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: APP_ROUTES.volume
  },
  {
    path: APP_ROUTES.volume,
    pathMatch: 'full',
    loadChildren: () => import('./volume/volume.module').then(m => m.VolumeModule)
  },
  {
    path: APP_ROUTES.auth,
    pathMatch: 'full',
    loadChildren: () => import('./core/core.module').then(m => m.CoreModule)
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: APP_ROUTES.volume
  }
];

export const AppRoutingModule = RouterModule.forRoot(appRoutes, {
  preloadingStrategy: PreloadAllModules
});
