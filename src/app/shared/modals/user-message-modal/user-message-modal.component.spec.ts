import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMessageModalComponent } from './user-message-modal.component';

describe('UserMessageModalComponent', () => {
  let component: UserMessageModalComponent;
  let fixture: ComponentFixture<UserMessageModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMessageModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMessageModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
