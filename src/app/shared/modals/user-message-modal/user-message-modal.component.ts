import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-user-message-modal',
  templateUrl: './user-message-modal.component.html',
  styleUrls: ['./user-message-modal.component.scss']
})
export class UserMessageModalComponent {

  message: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) message: string
  ) {
    this.message = message;
  }
}
