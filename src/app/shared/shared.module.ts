import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDialogComponent } from './modals/confirm-dialog/confirm-dialog.component';
import { JoinWithCommaPipe } from './pipes/join-with-comma.pipe';
import { RemoveUnderscorePipe } from './pipes/remove-underscore.pipe';
import { RemoveHtmlTagsPipe } from './pipes/remove-html-tags.pipe';
import { GetLabelClassPipe } from './pipes/get-label-class.pipe';
import { UserMessageModalComponent } from './modals/user-message-modal/user-message-modal.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    ConfirmDialogComponent,
    JoinWithCommaPipe,
    RemoveUnderscorePipe,
    RemoveHtmlTagsPipe,
    GetLabelClassPipe,
    UserMessageModalComponent,
  ],
  imports: [CommonModule],
  exports: [
    ConfirmDialogComponent,
    JoinWithCommaPipe,
    RemoveUnderscorePipe,
    RemoveHtmlTagsPipe,
    GetLabelClassPipe,
    UserMessageModalComponent,
    RouterModule
  ],
})
export class SharedModule {}
