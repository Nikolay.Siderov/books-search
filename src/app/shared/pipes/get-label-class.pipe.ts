import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getLabelClass'
})
export class GetLabelClassPipe implements PipeTransform {

  transform(value: string): string {
    let labelClass = 'label ';

    switch (value) {
      case 'FOR_SALE': labelClass += 'label--for-sale' ; break;
      case 'NOT_FOR_SALE': labelClass += 'label--not-for-sale' ; break;
      case 'FREE': labelClass += 'label--free' ; break;
    }

    return labelClass;
  }

}
