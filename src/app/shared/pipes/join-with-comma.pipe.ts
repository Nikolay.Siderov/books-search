import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'joinWithComma'
})
export class JoinWithCommaPipe implements PipeTransform {

  transform(value: Array<string>): string {
    return value ? value.join(', ') : '';
  }

}
