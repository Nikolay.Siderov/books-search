import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { loadMyVolumes } from './redux-store/my-volumes/actions/my-volume.actions';
import { IAppState } from './redux-store/global.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private store: Store<IAppState>) { }

  ngOnInit(): void {
    this.store.dispatch(loadMyVolumes());
  }
}
