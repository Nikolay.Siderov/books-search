import { createAction, props } from '@ngrx/store';
import { IMyVolumesCollection, IMyVolume } from 'src/app/volume/entities/my-volume.interface';

export const loadMyVolumes = createAction(
    '[My Volumes] Load Volumes'
);

export const loadMyVolumesSuccess = createAction(
    '[My Volumes] Load Volumes Success',
    props<{payload: IMyVolumesCollection}> ()
);

export const addVolume = createAction(
    '[My Volumes] Add To My Volumes',
    props<{payload: IMyVolume}> ()
);

export const addVolumeSuccess = createAction(
    '[My Volumes] Add To My Volumes Success',
    props<{payload: IMyVolume}> ()
);

export const deleteVolume = createAction(
    '[My Volumes] Delete Volume',
    props<{payload: string}> ()
);

export const deleteVolumeSuccess = createAction(
    '[My Volumes] Delete Volume Success',
    props<{payload: string}> ()
);

export const clearMyVolumes = createAction(
    '[My Volumes] Clear'
);

export const addVolumeError = createAction(
    '[My Volumes] Add Error',
    props<{payload: any}> ()
);
