import { ActionReducerMap, createReducer, on } from '@ngrx/store';
import { loadMyVolumesSuccess, deleteVolumeSuccess, addVolumeSuccess, clearMyVolumes } from '../actions/my-volume.actions';
import { IAppState, IAction, IMyVolumesState } from '../../global.interface';
import { IMyVolumesCollection, IMyVolume } from 'src/app/volume/entities/my-volume.interface';

const initialVolumesState: IMyVolumesCollection = {
    myVolumes: null
};

export const myVolumesReducer = createReducer<any>(
    initialVolumesState,

    on(loadMyVolumesSuccess, (state: IMyVolumesState, data: IAction<IMyVolumesCollection> ) => {

        const myVolumes = { ...data.payload };

        return {...state, myVolumes};
    }),

    on(deleteVolumeSuccess, (state: IMyVolumesState, volumeId: IAction<string>) => {

        const removeVolumeId: string = volumeId.payload;
        const myVolumes = {};

        for (const [key, value] of Object.entries(state.myVolumes)) {
            if (key !== removeVolumeId) {
                myVolumes[key] = value;
            }
        }

        return {...state, myVolumes};
    }),

    on(addVolumeSuccess, (state: IMyVolumesState, data: IAction<IMyVolume>) => {

        const myVolumes = {...state.myVolumes, [data.payload.id]: data.payload};

        return {...state, myVolumes};
    }),

    on(clearMyVolumes, () => {
        return {};
    })
);

export const reducers: ActionReducerMap<IAppState> = { global: myVolumesReducer };
