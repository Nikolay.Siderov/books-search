import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { loadMyVolumes, loadMyVolumesSuccess, addVolume, deleteVolume, deleteVolumeSuccess, addVolumeSuccess, addVolumeError } from '../actions/my-volume.actions';
import { mergeMap, map, catchError, switchMap, debounceTime} from 'rxjs/operators';
import { IAction } from '../../global.interface';
import { IMyVolumesCollection, IMyVolume } from 'src/app/volume/entities/my-volume.interface';
import { MyVolumesService } from 'src/app/volume/services/my-volumes.service';
import { of } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class MyVolumesEffects {

    getMyVolumes$ = createEffect(() => this.actions$.pipe(
        ofType(loadMyVolumes.type),
        switchMap(() => this.service.getMyVolumes()
            .pipe(
                map((payload: IMyVolumesCollection) => loadMyVolumesSuccess({payload}))
            )
        )
    ));

    addToMyVolumes$ = createEffect(() => this.actions$.pipe(
        ofType(addVolume.type),
        debounceTime(500),
        switchMap((data: IAction<IMyVolume>) => this.service.addVolume(data.payload)
            .pipe(
                map(() => {
                    this.service.errorEmitter.emit(false);
                    return addVolumeSuccess({payload: data.payload});
                }),
                catchError((error: HttpErrorResponse) => {
                    throw of(addVolumeError({payload: error}));
                })
            )
        )
    ));

    deleteVolume$ = createEffect(() => this.actions$.pipe(
        ofType(deleteVolume.type),
        switchMap((data: IAction<string>) => this.service.deleteVolume(data.payload)
            .pipe(
                map(() => deleteVolumeSuccess({payload: data.payload}))
            )
        )
    ));

    constructor(
        private actions$: Actions,
        private service: MyVolumesService
    ) { }
}
