import { IMyVolumesCollection } from '../volume/entities/my-volume.interface';

export interface IAppState {
  global: IMyVolumesState;
}

export interface IAction<T> {
  payload: T;
  type?: string;
}

export interface IMyVolumesState {
  myVolumes: IMyVolumesCollection;
}
