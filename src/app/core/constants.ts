import { MatDialogConfig } from '@angular/material/dialog';

export const API = {
    base: {
        url: 'https://www.googleapis.com/books/v1',
    },
    myVolumes: {
        url: 'https://book-library-9bca1.firebaseio.com',
        key: 'AIzaSyB_SfeLENmQKY3stWWPQNVE5F64MBMXWqU'
    }
};

export const HTTP_PATHS = {
    volume: '/volumes',
    myBooks: '/my-books'
};

export const API_AUTH = {
    url: 'https://identitytoolkit.googleapis.com/v1/',
    routes: {
        signIn: 'accounts:signInWithPassword',
    },
    key: 'AIzaSyB_SfeLENmQKY3stWWPQNVE5F64MBMXWqU'
};

/* *
* The 'params' property is used as constant in the routing as ( /: ) param variable and is used in the app to access these params
*/

export const APP_ROUTES = {
    auth: 'auth',
    volume: 'volume',
    login: 'login',
    home: 'home',
    search: 'search',
    details: 'details',
    myVolumes: 'my-volumes',
    params: {
        volumeId: 'volumeId',
        searchValue: 'searchValue'
    }
};

export const QUERY = {
    key: '?key=',
    apiKey: '?api_key=',
    page: '&page=',
    query: '?q=',
    json: '.json',
    filters: {
        maxResults: {
            type: 'maxResults',
            query: '&maxResults=',
            values: ['10', '20', '30', '40'],
            defaultValue: '10'
        },
        orderBy: {
            type: 'orderBy',
            query: '&orderBy=',
            values: ['relevance', 'newest'],
            defaultValue: 'relevance'
        },
        filter: {
            type: 'filter',
            query: '&filter=',
            values: ['partial', 'full', 'ebooks', 'free-ebooks', 'paid-ebooks'],
            defaultValue: ''
        },
        printType: {
            type: 'printType',
            query: '&printType=',
            values: ['all', 'books', 'magazines'],
            defaultValue: 'all'
        }
    }
};

// Used as default/primary configuration for modals
export const primaryModalConfig = new MatDialogConfig();
primaryModalConfig.width = '80%';
primaryModalConfig.maxWidth = '1100px';
primaryModalConfig.maxHeight = '95vh';
primaryModalConfig.autoFocus = false;

export const VOLUMES = 'volumes';
export const LOCALID = 'localId';
export const TOKEN = 'TOKEN';
export const EMAIL_REGEX = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/i;
export const DATE_REGEX = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
