import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {

  loggedUser$: BehaviorSubject<any>;
  @ViewChild('hamburgerBtn') hamburgerBtn: ElementRef;

  constructor( private authService: AuthenticationService ) {}

  ngOnInit(): void {
    this.loggedUser$ = this.authService.isUserLogged;
  }

  logout(): void {
    this.authService.logout();
  }

  hideHamburger(): void {
    this.hamburgerBtn.nativeElement.checked = true;
  }
}
