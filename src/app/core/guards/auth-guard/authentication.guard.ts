import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { APP_ROUTES } from '../../constants';

@Injectable()
export class AuthenticationGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthenticationService
  ) {}

  canActivate(): boolean {

    if (this.authService.token) {
      return true;
    } else {
      this.router.navigate([APP_ROUTES.login]);
      return false;
    }
  }
}
