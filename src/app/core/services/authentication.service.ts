import { Injectable, Output, EventEmitter } from '@angular/core';
import { API_AUTH, TOKEN, QUERY, LOCALID } from '../constants';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ILoginForm, ILoginRequestResponse } from '../entities/login.interface';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { IAppState } from 'src/app/redux-store/global.interface';
import { clearMyVolumes, loadMyVolumes } from 'src/app/redux-store/my-volumes/actions/my-volume.actions';

@Injectable()
export class AuthenticationService {

  @Output()
  public isUserLogged: BehaviorSubject<any> = new BehaviorSubject(this.token);

  @Output()
  public loginFail: EventEmitter<any> = new EventEmitter();

  constructor(
    private http: HttpClient,
    private router: Router,
    private store: Store<IAppState>
  ) {}

  login(loginRequest: ILoginForm): void {

    this.http
      .post( `${API_AUTH.url}${API_AUTH.routes.signIn}${QUERY.key}${API_AUTH.key}`, loginRequest )
      .subscribe(
        (success: ILoginRequestResponse) => {

          sessionStorage.setItem(TOKEN, success.idToken);
          sessionStorage.setItem(LOCALID, success.localId);
          this.isUserLogged.next(true);
          this.store.dispatch(loadMyVolumes());
          this.router.navigateByUrl('/');
        },

        (error: HttpErrorResponse) => {
          this.loginFail.emit(true);
        }
      );
  }

  logout(): void {
    sessionStorage.clear();
    this.isUserLogged.next(false);
    this.store.dispatch(clearMyVolumes());
  }

  get token(): string {
    return sessionStorage.getItem(TOKEN);
  }

  get localId(): string {
    return sessionStorage.getItem(LOCALID);
  }
}
