import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { APP_ROUTES } from './constants';

const coreRoutes: Routes = [
  {
    path: APP_ROUTES.auth,
    pathMatch: 'full',
    redirectTo: APP_ROUTES.login
  },
  {
    path: APP_ROUTES.login,
    pathMatch: 'full',
    component: LoginComponent
  }
];

export const CoreRoutingModule = RouterModule.forChild(coreRoutes);
