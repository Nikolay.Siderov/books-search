import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { EMAIL_REGEX } from '../../constants';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup;
  loginFail: boolean;
  loginFail$: Subscription;

  constructor(
    private fb: FormBuilder,
    public authService: AuthenticationService
  ) {}

  ngOnInit(): void {

    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(EMAIL_REGEX)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });

    this.loginFail$ = this.authService.loginFail.subscribe(
      (response: boolean) => {
        this.loginFail = response;
      }
    );
  }

  get validPassword(): boolean {
    return (
      this.loginForm.controls.password.touched &&
      this.loginForm.controls.password.invalid
    );
  }

  get validEmail(): boolean {
    return (
      this.loginForm.controls.email.touched &&
      this.loginForm.controls.email.invalid
    );
  }

  submitLoginForm(): void {
    this.authService.login(this.loginForm.value);
  }

  ngOnDestroy() {
    this.loginFail$.unsubscribe();
  }
}
