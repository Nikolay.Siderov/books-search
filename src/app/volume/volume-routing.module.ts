import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './pages/search/search.component';
import { APP_ROUTES } from '../core/constants';
import { ResultsComponent } from './pages/results/results.component';
import { VolumeDetailsComponent } from './pages/volume-details/volume-details.component';
import { MyVolumesComponent } from './pages/my-volumes/my-volumes.component';
import { AuthenticationGuard } from '../core/guards/auth-guard/authentication.guard';
import { MyVolumesGuard } from './guards/my-volumes.guard';

const booksRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: APP_ROUTES.home,
      },
      {
        path: APP_ROUTES.home,
        pathMatch: 'full',
        component: SearchComponent,
      },
    ],
  },
  {
    path: APP_ROUTES.volume,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '/' + APP_ROUTES.home,
      },
      {
        path: APP_ROUTES.myVolumes,
        canActivate: [AuthenticationGuard],
        pathMatch: 'full',
        component: MyVolumesComponent,
      },
      {
        path: APP_ROUTES.search + '/:' + APP_ROUTES.params.searchValue,
        pathMatch: 'full',
        component: ResultsComponent,
      },
      {
        path: APP_ROUTES.details + '/:' + APP_ROUTES.params.volumeId,
        canActivate: [MyVolumesGuard],
        pathMatch: 'full',
        component: VolumeDetailsComponent,
      },
    ],
  },
];

export const BooksRoutinModule = RouterModule.forChild(booksRoutes);
