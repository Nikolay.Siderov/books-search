import { NgModule, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchComponent } from './pages/search/search.component';
import { BooksRoutinModule } from './volume-routing.module';
import { VolumesService } from './services/volumes.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookCardComponent } from './components/volume-card/volume-card.component';
import { ResultsComponent } from './pages/results/results.component';
import { MyVolumesComponent } from './pages/my-volumes/my-volumes.component';
import { MyVolumesService } from './services/my-volumes.service';
import { ManageVolumeModalComponent } from './modals/manage-volume-modal/manage-volume-modal.component';
import { PredefinedVolumesSearchComponent } from './components/search-form/predefined-volumes-search/predefined-volumes-search.component';
import { EffectsModule } from '@ngrx/effects';
import { MyVolumesEffects } from '../redux-store/my-volumes/effects/my-volumes.effects';
import { MyVolumesGuard } from './guards/my-volumes.guard';
import { VolumeDetailsComponent } from './pages/volume-details/volume-details.component';
import { SharedModule } from '../shared/shared.module';
import { VolumesErrorHandler } from './volumes-module.error-handler';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    SearchFormComponent,
    SearchComponent,
    BookCardComponent,
    ResultsComponent,
    VolumeDetailsComponent,
    MyVolumesComponent,
    ManageVolumeModalComponent,
    PredefinedVolumesSearchComponent,
  ],
  imports: [
    CommonModule,
    BooksRoutinModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    EffectsModule.forFeature([MyVolumesEffects]),
    BrowserAnimationsModule,
  ],
  providers: [
    VolumesService,
    MyVolumesService,
    MyVolumesGuard,
    {
      provide: ErrorHandler,
      useClass: VolumesErrorHandler,
    },
  ],
})
export class VolumeModule {}
