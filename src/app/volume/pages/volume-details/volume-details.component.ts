import { Component, OnInit } from '@angular/core';
import { VolumesService } from '../../services/volumes.service';
import { ActivatedRoute } from '@angular/router';
import { IVolumeInfo } from '../../entities/volume-info.interface';
import { ISaleInfo } from '../../entities/sale-info.interface';
import { IVolumeAccessInfo } from '../../entities/access-info.interface';
import { IVolumeItem } from '../../entities/book.interface';
import { APP_ROUTES } from 'src/app/core/constants';

@Component({
  selector: 'app-volume-details',
  templateUrl: './volume-details.component.html',
  styleUrls: ['./volume-details.component.scss']
})
export class VolumeDetailsComponent implements OnInit {
  volumeId: string;
  volume: IVolumeInfo;
  volumeSaleInfo: ISaleInfo;
  volumeAccessInfo: IVolumeAccessInfo;

  constructor(
    private service: VolumesService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.volumeId = this.route.snapshot.paramMap.get(APP_ROUTES.params.volumeId);

    this.service
      .getVolumeById(this.volumeId)
      .subscribe((volumeData: IVolumeItem) => {
        this.volume = volumeData.volumeInfo;
        this.volumeSaleInfo = volumeData.saleInfo;
        this.volumeAccessInfo = volumeData.accessInfo;
      });
  }

  get downloadLink(): string {
    let downloadLink: string;

    if (this.volumeAccessInfo?.pdf?.acsTokenLink) {
      downloadLink = this.volumeAccessInfo?.pdf?.acsTokenLink;
    }else if (this.volumeAccessInfo?.pdf?.downloadLink) {
      downloadLink = this.volumeAccessInfo?.pdf?.downloadLink;
    }

    return downloadLink;
  }
}
