import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { VolumesService } from '../../services/volumes.service';
import { IQueryParams } from '../../entities/search-query.interface';
import { IVolumeItem } from '../../entities/book.interface';
import { APP_ROUTES } from 'src/app/core/constants';
import { IHttpResponseVolumesSearch } from '../../entities/search-http-response.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
})
export class ResultsComponent implements OnInit, OnDestroy {

  searchValue: string;
  queryParams: IQueryParams;
  volumesList: Array<IVolumeItem>;

  subscriptionParam$: Subscription;
  subscriptionQuery$: Subscription;

  constructor(
    private router: ActivatedRoute,
    private service: VolumesService
  ) {}

  ngOnInit(): void {

    this.subscriptionParam$ =
      this.router.paramMap.subscribe((params: ParamMap) => {

        this.searchValue = params.get(APP_ROUTES.params.searchValue);

        this.subscriptionQuery$ =
          this.router.queryParams.subscribe( (queryParams: IQueryParams) => {

            this.queryParams = queryParams;

            this.service.search(this.searchValue, this.queryParams)
              .subscribe((response: IHttpResponseVolumesSearch) =>
                this.volumesList = response.items
              );
          }
        );

        if (!this.queryParams) {
          this.service.search(this.searchValue)
            .subscribe( (response: IHttpResponseVolumesSearch) =>
              this.volumesList = response.items
            );
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.subscriptionParam$.unsubscribe();
    this.subscriptionQuery$.unsubscribe();
  }
}
