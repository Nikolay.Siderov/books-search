import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ManageVolumeModalComponent } from '../../modals/manage-volume-modal/manage-volume-modal.component';
import { primaryModalConfig } from 'src/app/core/constants';
import { Store } from '@ngrx/store';
import { IMyVolumesCollection } from '../../entities/my-volume.interface';
import { IAppState } from 'src/app/redux-store/global.interface';
import { ConfirmDialogComponent } from 'src/app/shared/modals/confirm-dialog/confirm-dialog.component';
import { deleteVolume } from 'src/app/redux-store/my-volumes/actions/my-volume.actions';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-my-volumes',
  templateUrl: './my-volumes.component.html',
  styleUrls: ['./my-volumes.component.scss']
})
export class MyVolumesComponent implements OnInit {

  volumesData$: Observable<IMyVolumesCollection>;

  constructor(
    private dialog: MatDialog,
    private store: Store<IAppState>
  ) { }

  ngOnInit(): void {
    this.volumesData$ = this.store.select(state => state.global.myVolumes);
  }

  openManageVolumeModal(): void {
    this.dialog.open(ManageVolumeModalComponent, primaryModalConfig);
  }

  deleteVolume(volumeId: string): void {
    this.dialog.open(ConfirmDialogComponent).afterClosed()
      .pipe(take(1))
      .subscribe((isConfirm: boolean) => {
        if (isConfirm) {
          this.store.dispatch(deleteVolume({payload: volumeId}));
        }
      });
  }
}
