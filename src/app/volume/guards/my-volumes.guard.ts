import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router';
import { APP_ROUTES } from 'src/app/core/constants';

@Injectable()
export class MyVolumesGuard implements CanActivate {

  canActivate(): boolean {
    return !this.router.url.includes(APP_ROUTES.myVolumes);
  }

  constructor( private router: Router ) { }
}
