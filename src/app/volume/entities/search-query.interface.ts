export interface IQueryParams {
    maxResults?: string;
    orderBy?: string;
    filter?: string;
    printType?: string;
}
