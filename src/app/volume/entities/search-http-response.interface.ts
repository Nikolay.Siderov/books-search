import { IVolumeItem } from './book.interface';

export interface IHttpResponseVolumesSearch{
    items: Array<IVolumeItem>;
    kind: string;
    totalItems: number;
}
