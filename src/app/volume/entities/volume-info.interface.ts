export interface IVolumeInfo{
    title: string;
    allwAnonLogging: boolean;
    authors: Array<string>;
    averageRating: number;
    canonicalVolumeLink: string;
    categories: Array<string>;
    contentVersion: string;
    description: string;

    imageLinks: {
        extraLarge?: string;
        large?: string;
        medium?: string;
        small?: string;
        thumbnail?: string;
        smallThumbnail?: string;

    };

    industryIdentifiers: Array<any>;
    language: string;
    maturityRating: string;
    pageCount: number;
    previewLink: string;
    printType: string;
    publishedDate: string;
    publisher: string;
    ratingsCount: number;
    readingModers: object;
    subtitle: string;
}
