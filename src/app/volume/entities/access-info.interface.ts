export interface IVolumeAccessInfo {
  accessViewStatus: string;
  country: string;
  epub: any;
  pdf: {
    downloadLink?: string;
    acsTokenLink?: string;
    isAvailable: boolean;
  };
}
