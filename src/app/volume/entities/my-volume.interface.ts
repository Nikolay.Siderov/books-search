export interface IMyVolume {
  id: string;
  volumeInfo: {
    title: string;
    authors: Array<string>;
    publisher: string;
    publishedDate: string;
    pageCount: number;
    imageLinks: {
        thumbnail: string;
    };
  };
}

export interface IMyVolumesCollection {
  [key: string]: IMyVolume;
}
