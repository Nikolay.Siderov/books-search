export interface ISaleInfo{
    buyLink: string;
    country: string;
    isEbook: boolean;

    listPrice: {
        amount: number;
        currencyCode: string;
    };

    offers: Array<any>;
    retailPrice: {
        amount: number;
        currencyCode: string;
    };

    saleability: string;
}
