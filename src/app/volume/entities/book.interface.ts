import { IVolumeInfo } from './volume-info.interface';
import { ISaleInfo } from './sale-info.interface';
import { IVolumeAccessInfo } from './access-info.interface';

export interface IVolumeItem{
    accessInfo: IVolumeAccessInfo;
    etag: string;
    id: string;
    kind: string;
    saleInfo: ISaleInfo;
    searchInfo: object;
    selfLink: string;
    volumeInfo: IVolumeInfo;
}
