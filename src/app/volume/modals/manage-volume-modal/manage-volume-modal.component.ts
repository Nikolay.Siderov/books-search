import { Component, OnInit, Inject, Optional } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { IMyVolume } from '../../entities/my-volume.interface';
import { DATE_REGEX } from 'src/app/core/constants';
import { Store } from '@ngrx/store';
import { addVolume } from 'src/app/redux-store/my-volumes/actions/my-volume.actions';
import { IAppState } from 'src/app/redux-store/global.interface';
import { MyVolumesService } from '../../services/my-volumes.service';


@Component({
  selector: 'app-manage-volume-modal',
  templateUrl: './manage-volume-modal.component.html',
  styleUrls: ['./manage-volume-modal.component.scss'],
})
export class ManageVolumeModalComponent implements OnInit {

  volumeManageForm: FormGroup;
  myVolumeData?: IMyVolume;
  requestFail: boolean;
  requestSuccess: boolean;
  operationTypeText: string;

  constructor(
    private fb: FormBuilder,
    private store: Store<IAppState>,
    private dialog: MatDialogRef<ManageVolumeModalComponent>,
    private myVolumeService: MyVolumesService,
    @Optional() @Inject(MAT_DIALOG_DATA) data: IMyVolume,
  ) {
    this.myVolumeData = data;
  }

  // Checks if there is volume data fills the form with the passed data

  ngOnInit(): void {

    this.operationTypeText = this.myVolumeData ? 'Update' : 'Create';
    const id = this.myVolumeData?.id;
    const title = this.myVolumeData?.volumeInfo.title;
    const publisher = this.myVolumeData?.volumeInfo.publisher;
    const publishedDate = this.myVolumeData?.volumeInfo.publishedDate;
    const pageCount = this.myVolumeData?.volumeInfo.pageCount;
    const authors = this.myVolumeData?.volumeInfo.authors?.join(', ');
    const imageLinks = this.myVolumeData?.volumeInfo.imageLinks.thumbnail;

    this.volumeManageForm = this.fb.group({
      title: [title, Validators.required],
      volumeId: [id, Validators.required],
      authors: [authors, Validators.required],
      publisher: [publisher, Validators.required],
      publishedDate: [publishedDate, Validators.required],
      pageCount: [pageCount, Validators.required],
      imageLinks: [imageLinks, Validators.required],
    });
  }

  // Initialize the form validators

  get validTitle(): boolean {
    return (
      this.volumeManageForm.controls.title.invalid &&
      this.volumeManageForm.controls.title.touched
    );
  }

  get validVolumeId(): boolean {
    return (
      this.volumeManageForm.controls.volumeId.invalid &&
      this.volumeManageForm.controls.volumeId.touched
    );
  }

  get validAuthors(): boolean {
    return (
      this.volumeManageForm.controls.authors.invalid &&
      this.volumeManageForm.controls.authors.touched
    );
  }

  get validPublisher(): boolean {
    return (
      this.volumeManageForm.controls.publisher.invalid &&
      this.volumeManageForm.controls.publisher.touched
    );
  }

  get validPublishedDate(): boolean {

    const isDateValid = DATE_REGEX.test(
      this.volumeManageForm.value.publishedDate
    );

    if (!isDateValid) {
      this.volumeManageForm.controls.publishedDate.patchValue(null);
    }

    return (
      this.volumeManageForm.controls.publishedDate.invalid &&
      this.volumeManageForm.controls.publishedDate.touched
    );
  }

  get validPageCount(): boolean {
    return (
      this.volumeManageForm.controls.pageCount.invalid &&
      this.volumeManageForm.controls.pageCount.touched
    );
  }

  get validImageLinks(): boolean {
    return (
      this.volumeManageForm.controls.imageLinks.invalid &&
      this.volumeManageForm.controls.imageLinks.touched
    );
  }

  submitManageForm(): void {

    const payload = this.generateSubmiteBody();
    this.handleResponse();
    this.store.dispatch(addVolume({payload}));
  }

  generateSubmiteBody(): IMyVolume {

    const authors: Array<string> = this.volumeManageForm.value.authors
      .split(',')
      .map((x: string) => x.trim());

    // Gets the form input information (spliting the form into groups is skipped for code simplicity)
    const { title, publisher, publishedDate, pageCount } = this.volumeManageForm.value;
    const thumbnail: string = this.volumeManageForm.value.imageLinks;
    const id = this.volumeManageForm.value.volumeId;

    return {
      id,
      volumeInfo: {
        title,
        authors,
        publisher,
        publishedDate,
        pageCount,
        imageLinks: { thumbnail },
      },
    };
  }

  /* ERROR HANDLING
  * In the feature can be updated to return UX friendly response (only if there is adequate API response)
  * Error handling its implemented only partialy and its done only for demonstration and slightly better UX
  */

  handleResponse(): void {

    this.myVolumeService.errorEmitter.subscribe((errorResponse: string) => {

      if (errorResponse) {
        this.requestFail = true;

      }else {
        this.requestFail = false;
        this.requestSuccess = true;
        this.volumeManageForm.reset();

        setTimeout(() => {
          this.dialog.close();
        }, 1500);
      }
    });

  }
}
