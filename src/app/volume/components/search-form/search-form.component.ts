import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { QUERY, APP_ROUTES } from 'src/app/core/constants';
import { Router, ActivatedRoute } from '@angular/router';
import { IQueryParams } from '../../entities/search-query.interface';
import { advancedSearchAnimations } from './search-form.animations';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
  animations: [advancedSearchAnimations]
})
export class SearchFormComponent implements OnInit, OnDestroy {

  searchForm: FormGroup;
  isFiltersToggled = false;
  routerQuerys$: Subscription;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRouter: ActivatedRoute
  ) {}

  // Checks the router for query params or set default values

  ngOnInit(): void {

    this.routerQuerys$ = this.activatedRouter.queryParams.subscribe(query => {
      this.isFiltersToggled = Object.keys(query).length > 0;
    });

    const { searchValue }  = this.activatedRouter.snapshot.params;
    const { maxResults, orderBy, printType, filter } = this.activatedRouter.snapshot.queryParams;

    this.searchForm = this.fb.group({
      searchValue: [searchValue, [Validators.required]],
      filters: this.fb.group({
        maxResults: [maxResults || QUERY.filters.maxResults.defaultValue],
        orderBy: [orderBy || QUERY.filters.orderBy.defaultValue],
        printType: [printType || QUERY.filters.printType.defaultValue],
        filter: [filter || QUERY.filters.filter.defaultValue]
      }),
    });
  }

  toggleFilters(): void {
    this.isFiltersToggled = !this.isFiltersToggled;
  }

  // Gets the value from the search form and navigates to the result page

  submitForm(): void {
    if (!this.searchForm.invalid) {
      const { searchValue } = this.searchForm.value;

      this.router.navigate(
        [APP_ROUTES.volume + '/' + APP_ROUTES.search, searchValue],
        {
          queryParams: this.generateQueryParams(),
        }
      );
    }
  }

  /* *
   * 1. Gets the filter values from the form
   * 2. Checks them are they match with the available query params from the constants
   * 3. Reduces them if they match with the default
   */

  generateQueryParams(): IQueryParams {
    const formFilters: IQueryParams = this.searchForm.get('filters').value;

    const queryFilters: IQueryParams = Object.entries(formFilters).reduce(
      (acc, [queryParam, queryValue]) => {
        if (
          queryValue &&
          QUERY.filters[queryParam].defaultValue !== String(queryValue)
        ) {
          return { ...acc, [queryParam]: queryValue };
        }

        return acc;
      }, {});

    return queryFilters;
  }

  ngOnDestroy(): void {
    this.routerQuerys$.unsubscribe();
  }
}
