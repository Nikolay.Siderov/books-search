import { trigger, state, style, animate, transition } from '@angular/animations';

export const advancedSearchAnimations =
trigger('advancedSearch', [
  state('open', style({
    minHeight: '60px',
    opacity: 1,
    })),
  state('close', style({
    height: 0,
    opacity: 0,
    'z-index': '-10'
  })),
    transition('* <=> open', [
      animate('.3s')
    ])
]);
