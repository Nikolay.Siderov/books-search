import { Component } from '@angular/core';

@Component({
  selector: 'app-predefined-volumes-search',
  templateUrl: './predefined-volumes-search.component.html',
  styleUrls: ['./predefined-volumes-search.component.scss']
})
export class PredefinedVolumesSearchComponent { }
