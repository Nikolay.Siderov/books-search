import { Component, Input, OnInit } from '@angular/core';
import { IVolumeItem } from '../../entities/book.interface';
import { MatDialog } from '@angular/material/dialog';
import { ManageVolumeModalComponent } from '../../modals/manage-volume-modal/manage-volume-modal.component';
import { primaryModalConfig } from 'src/app/core/constants';
import { IMyVolume } from '../../entities/my-volume.interface';
import { AuthenticationService } from 'src/app/core/services/authentication.service';


@Component({
  selector: 'app-volume-card',
  templateUrl: './volume-card.component.html',
  styleUrls: ['./volume-card.component.scss'],
})
export class BookCardComponent implements OnInit {

  isUserLogged: boolean;
  @Input() volume: IVolumeItem;

  constructor( private dialog: MatDialog, private auth: AuthenticationService ) {}

  ngOnInit(): void {
    this.isUserLogged = !!this.auth.token;
  }

  openManageDialog(): void {

    // Creates the data which is passed to the modal

    const data: IMyVolume = {
      id: this.volume.id,
      volumeInfo: {
        title: this.volume.volumeInfo.title,
        authors: this.volume.volumeInfo.authors,
        publisher: this.volume.volumeInfo.publisher,
        publishedDate: this.volume.volumeInfo.publishedDate,
        pageCount: this.volume.volumeInfo.pageCount,
        imageLinks: {
          thumbnail: this.volume.volumeInfo.imageLinks.thumbnail
        }
      }
    };

    this.dialog.open(ManageVolumeModalComponent, { ...primaryModalConfig, data });
  }
}
