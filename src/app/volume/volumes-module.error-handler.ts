import { ErrorHandler, Injectable, Injector, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { IAction } from '../redux-store/global.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { MyVolumesService } from './services/my-volumes.service';
import { MatDialog } from '@angular/material/dialog';
import { UserMessageModalComponent } from '../shared/modals/user-message-modal/user-message-modal.component';
import { Router } from '@angular/router';

@Injectable()
export class VolumesErrorHandler implements ErrorHandler {

  constructor(
    private injector: Injector,
    private zone: NgZone
  ) { }

  handleError(error: Observable<IAction<HttpErrorResponse>> | HttpErrorResponse): void {

    if (error instanceof Observable) {

      error.pipe(take(1)).subscribe((response: IAction<HttpErrorResponse>) => { // Observables thrown from redux effects
        this.myVolumeService.errorEmitter.emit(response.type);
      });

    }else if (error instanceof HttpErrorResponse) {

      const message = error?.error?.error?.message || 'Something went wrong';

      if (error.status < 200 || error.status >= 400) {

        this.zone.run(() => this.dialog.open(UserMessageModalComponent, {
          data: message
        }));

        this.zone.run(() => this.router.navigateByUrl('/'));
      }
    }
  }

  // Injection has to be done because the error handler lifecycle hook is before DI hooks
  get router(): Router {
    return this.injector.get(Router);
  }

  get myVolumeService(): MyVolumesService {
    return this.injector.get(MyVolumesService);
  }

  get dialog(): MatDialog {
    return this.injector.get(MatDialog);
  }
}
