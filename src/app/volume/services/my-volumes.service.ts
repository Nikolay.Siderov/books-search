import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { API, HTTP_PATHS, QUERY } from 'src/app/core/constants';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { take } from 'rxjs/operators';
import { IMyVolume, IMyVolumesCollection } from '../entities/my-volume.interface';

@Injectable()
export class MyVolumesService {

  @Output()
  public errorEmitter: EventEmitter<string | boolean> = new EventEmitter();

  constructor(private http: HttpClient, private auth: AuthenticationService) {}

  getMyVolumes(): Observable<IMyVolumesCollection> {

    return this.http.get<IMyVolumesCollection> (
      API.myVolumes.url + HTTP_PATHS.myBooks + '/' + this.auth.localId +
      QUERY.json + QUERY.key + API.myVolumes.key )
      .pipe(take(1));

  }

  addVolume(volume: IMyVolume): Observable<IMyVolume> {

    return this.http.put<IMyVolume>(
      API.myVolumes.url + HTTP_PATHS.myBooks + '/' +  this.auth.localId + '/' +
      volume.id + QUERY.json + QUERY.key + API.myVolumes.key, volume )
      .pipe(take(1));
  }

  deleteVolume(volumeId: string): Observable<any> {

    return this.http.delete(
      API.myVolumes.url + HTTP_PATHS.myBooks + '/' + this.auth.localId + '/' +
      volumeId +  QUERY.json + QUERY.key +  API.myVolumes.key )
    .pipe(take(1));
  }
}
