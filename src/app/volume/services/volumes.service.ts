import { Injectable } from '@angular/core';
import { API, HTTP_PATHS, QUERY } from '../../core/constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IHttpResponseVolumesSearch } from '../entities/search-http-response.interface';
import { IQueryParams } from '../entities/search-query.interface';
import { take } from 'rxjs/operators';

@Injectable()
export class VolumesService {

  constructor( private http: HttpClient ) { }

  search(searchValue: string, params: IQueryParams = {}): Observable<IHttpResponseVolumesSearch> {

    const url: string = API.base.url + HTTP_PATHS.volume + QUERY.query + searchValue;
    let queryParams = '';

    for (const [queryParam, queryValue] of Object.entries(params)){

      if (QUERY.filters.hasOwnProperty(queryParam) && queryValue){
        queryParams += QUERY.filters[queryParam].query + queryValue;
      }
    }

    return this.http.get<IHttpResponseVolumesSearch>(url + queryParams)
      .pipe(take(1));
  }

  getVolumeById(volumeId: string): Observable<any>{

    return this.http.get(API.base.url + HTTP_PATHS.volume + '/' + volumeId)
      .pipe(take(1));
  }
}
