Note: The 'home page' is located in volume/pages/search component,
the component name doesn't correspond with the URL route name with the reason to keep more correct and independant component naming
without strong relation with the routing names.

Also, its possible to "GET http://localhost:4200/:false-image-link 404 (Not Found)" in the console if there is image which is unavailable

All the constants used in the app are located in the 'core' folder

And... press the buttons gently - just in case to dont break something :)